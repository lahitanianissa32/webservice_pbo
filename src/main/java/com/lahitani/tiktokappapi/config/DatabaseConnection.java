package com.lahitani.tiktokappapi.config;

import com.google.cloud.firestore.Firestore;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.DependsOn;

@Component
@DependsOn("firebaseInitialization")
public class DatabaseConnection {
    private Firestore dbFirestore;

    public DatabaseConnection() {
        // Inisialisasi koneksi ke database di sini
        dbFirestore = FirestoreClient.getFirestore();
    }

    public Firestore getFirestore() {
        return dbFirestore;
    }
}


