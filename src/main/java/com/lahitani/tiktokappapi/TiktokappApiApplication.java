package com.lahitani.tiktokappapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TiktokappApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TiktokappApiApplication.class, args);
	}

}
