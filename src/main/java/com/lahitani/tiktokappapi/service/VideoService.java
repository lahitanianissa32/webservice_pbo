package com.lahitani.tiktokappapi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.lahitani.tiktokappapi.config.DatabaseConnection;
import com.lahitani.tiktokappapi.entity.Video;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.FieldValue;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;

@Service
public class VideoService {

    private static final String COLLECTION_NAME = "videos";
    private final Firestore dbFirestore;

    public VideoService(DatabaseConnection databaseConnection) {
        this.dbFirestore = databaseConnection.getFirestore();
    }

    public ResponseEntity<String> saveVideo(Video video) {
        try {
            dbFirestore.collection(COLLECTION_NAME)
                    .document(video.getUid())
                    .set(video);

            return ResponseEntity.ok("Video saved successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to save video: " + e.getMessage());
        }
    }

    public ResponseEntity<?> getVideoDetails(String uid) {
        try {
            DocumentReference documentReference = dbFirestore.collection(COLLECTION_NAME).document(uid);
            ApiFuture<DocumentSnapshot> future = documentReference.get();
            DocumentSnapshot documentSnapshot = future.get();

            if (documentSnapshot.exists()) {
                Video video = documentSnapshot.toObject(Video.class);
                return ResponseEntity.ok(video);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve video details: " + e.getMessage());
        }
    }

    public ResponseEntity<String> updateVideo(Video video) {
        try {
            Map<String, Object> updateData = video.toMap();
            updateData.remove("uid"); // Menghapus field UID dari data pembaruan

            // Menghapus data yang tidak berubah
            Map<String, Object> filteredData = new HashMap<>();
            for (Map.Entry<String, Object> entry : updateData.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                // Memastikan nilai tidak null
                if (value != null) {
                    filteredData.put(key, value);
                }
            }

            // Melakukan pembaruan hanya jika ada perubahan
            if (!filteredData.isEmpty()) {
                filteredData.put("lastUpdated", FieldValue.serverTimestamp()); // Opsional: Sertakan field timestamp

                dbFirestore.collection("videos")
                        .document(video.getUid())
                        .update(filteredData);

                return ResponseEntity.ok("Video updated successfully");
            }

            return ResponseEntity.ok("No changes to update");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to update video: " + e.getMessage());
        }
    }



    public ResponseEntity<String> deleteVideo(String uid) {
        try {
            dbFirestore.collection(COLLECTION_NAME)
                    .document(uid)
                    .delete();

            return ResponseEntity.ok("Video deleted successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to delete video: " + e.getMessage());
        }
    }

    public ResponseEntity<?> getAllVideos() {
        try {
            CollectionReference collectionReference = dbFirestore.collection(COLLECTION_NAME);
            ApiFuture<QuerySnapshot> future = collectionReference.get();
            QuerySnapshot querySnapshot = future.get();
            List<Video> videoList = new ArrayList<>();

            for (QueryDocumentSnapshot document : querySnapshot.getDocuments()) {
                Video video = document.toObject(Video.class);
                videoList.add(video);
            }

            return ResponseEntity.ok(videoList);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve videos: " + e.getMessage());
        }
    }
}
