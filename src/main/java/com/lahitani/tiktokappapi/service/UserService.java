package com.lahitani.tiktokappapi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.lahitani.tiktokappapi.config.DatabaseConnection;
import com.lahitani.tiktokappapi.entity.User;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.FieldValue;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;

@Service
public class UserService {

    private static final String COLLECTION_NAME = "users";
    private final Firestore dbFirestore;

    public UserService(DatabaseConnection databaseConnection) {
        this.dbFirestore = databaseConnection.getFirestore();
    }

    public ResponseEntity<String> saveUser(User user) {
        try {
            dbFirestore.collection(COLLECTION_NAME)
                    .document(user.getUid())
                    .set(user);

            return ResponseEntity.ok("User saved successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to save user: " + e.getMessage());
        }
    }

    public ResponseEntity<?> getUserDetails(String uid) {
        try {
            DocumentReference documentReference = dbFirestore.collection(COLLECTION_NAME).document(uid);
            ApiFuture<DocumentSnapshot> future = documentReference.get();
            DocumentSnapshot documentSnapshot = future.get();

            if (documentSnapshot.exists()) {
                User user = documentSnapshot.toObject(User.class);
                return ResponseEntity.ok(user);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve user details: " + e.getMessage());
        }
    }

    public ResponseEntity<String> updateUser(User user) {
        try {
            Map<String, Object> updateData = user.toMap();
            updateData.remove("uid"); // Menghapus field UID dari data pembaruan

            // Menghapus data yang tidak berubah
            Map<String, Object> filteredData = new HashMap<>();
            for (Map.Entry<String, Object> entry : updateData.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                // Memastikan nilai tidak null
                if (value != null) {
                    filteredData.put(key, value);
                }
            }

            // Melakukan pembaruan hanya jika ada perubahan
            if (!filteredData.isEmpty()) {
                filteredData.put("lastUpdated", FieldValue.serverTimestamp()); // Opsional: Sertakan field timestamp

                dbFirestore.collection("users")
                        .document(user.getUid())
                        .update(filteredData);

                return ResponseEntity.ok("User updated successfully");
            }

            return ResponseEntity.ok("No changes to update");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to update user: " + e.getMessage());
        }
    }



    public ResponseEntity<String> deleteUser(String uid) {
        try {
            dbFirestore.collection(COLLECTION_NAME)
                    .document(uid)
                    .delete();

            return ResponseEntity.ok("User deleted successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to delete user: " + e.getMessage());
        }
    }

    public ResponseEntity<?> getAllUsers() {
        try {
            CollectionReference collectionReference = dbFirestore.collection(COLLECTION_NAME);
            ApiFuture<QuerySnapshot> future = collectionReference.get();
            QuerySnapshot querySnapshot = future.get();
            List<User> userList = new ArrayList<>();

            for (QueryDocumentSnapshot document : querySnapshot.getDocuments()) {
                User user = document.toObject(User.class);
                userList.add(user);
            }

            return ResponseEntity.ok(userList);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve users: " + e.getMessage());
        }
    }
    public ResponseEntity<String> addFollower(String uid, User follower) {
        try {
            DocumentReference userRef = dbFirestore.collection(COLLECTION_NAME).document(uid);
            CollectionReference followersRef = userRef.collection("followers");

            // Menyimpan pengikut sebagai dokumen di dalam subkolom followers
            followersRef.document(follower.getUid()).set(follower);

            return ResponseEntity.ok("Follower added successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to add follower: " + e.getMessage());
        }
    }

    public ResponseEntity<String> addFollowing(String uid, User following) {
        try {
            DocumentReference userRef = dbFirestore.collection(COLLECTION_NAME).document(uid);
            CollectionReference followingRef = userRef.collection("following");

            // Menyimpan pengguna yang diikuti sebagai dokumen di dalam subkolom following
            followingRef.document(following.getUid()).set(following);

            return ResponseEntity.ok("Following added successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to add following: " + e.getMessage());
        }
    }


}
