package com.lahitani.tiktokappapi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.lahitani.tiktokappapi.config.DatabaseConnection;
import com.lahitani.tiktokappapi.entity.Comment;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.FieldValue;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;

@Service
public class CommentService {

    private static final String COLLECTION_NAME = "videos";

    private final Firestore dbFirestore;

    public CommentService(DatabaseConnection databaseConnection) {
        this.dbFirestore = databaseConnection.getFirestore();
    }

    public ResponseEntity<String> saveComment(String uid, Comment comment) {
        try {
            CollectionReference videosCollection = dbFirestore.collection(COLLECTION_NAME);
            DocumentReference videoDocRef = videosCollection.document(uid);
            CollectionReference commentsCollection = videoDocRef.collection("comments");

            String commentId = comment.getId();
            DocumentReference commentDocRef = commentsCollection.document(commentId);
            comment.setId(commentId);
            commentDocRef.set(comment);

            return ResponseEntity.ok("Comment saved successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to save comment: " + e.getMessage());
        }
    }


    public ResponseEntity<?> getCommentDetails(String uid) {
        try {
            CollectionReference videosCollection = dbFirestore.collection(COLLECTION_NAME);
            DocumentReference videoDocRef = videosCollection.document(uid);
            CollectionReference commentsCollection = videoDocRef.collection("comments");

            ApiFuture<QuerySnapshot> future = commentsCollection.get();
            QuerySnapshot querySnapshot = future.get();
            List<Comment> commentList = new ArrayList<>();

            for (QueryDocumentSnapshot document : querySnapshot.getDocuments()) {
                Comment comment = document.toObject(Comment.class);
                commentList.add(comment);
            }

            return ResponseEntity.ok(commentList);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve comment details: " + e.getMessage());
        }
    }

    public ResponseEntity<String> updateComment(Comment comment) {
        try {
            CollectionReference videosCollection = dbFirestore.collection(COLLECTION_NAME);
            DocumentReference videoDocRef = videosCollection.document(comment.getUid());
            CollectionReference commentsCollection = videoDocRef.collection("comments");

            DocumentReference commentDocRef = commentsCollection.document(comment.getId());
            Map<String, Object> updateData = comment.toMap();
            updateData.remove("uid"); // Menghapus field UID dari data pembaruan

            // Menghapus data yang tidak berubah
            Map<String, Object> filteredData = new HashMap<>();
            for (Map.Entry<String, Object> entry : updateData.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                // Memastikan nilai tidak null
                if (value != null) {
                    filteredData.put(key, value);
                }
            }

            // Melakukan pembaruan hanya jika ada perubahan
            if (!filteredData.isEmpty()) {
                filteredData.put("lastUpdated", FieldValue.serverTimestamp()); // Opsional: Sertakan field timestamp

                commentDocRef.update(filteredData);

                return ResponseEntity.ok("Comment updated successfully");
            }

            return ResponseEntity.ok("No changes to update");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to update comment: " + e.getMessage());
        }
    }

    public ResponseEntity<String> deleteComment(String uid, String commentId) {
        try {
            CollectionReference videosCollection = dbFirestore.collection(COLLECTION_NAME);
            DocumentReference videoDocRef = videosCollection.document(uid);
            CollectionReference commentsCollection = videoDocRef.collection("comments");

            DocumentReference commentDocRef = commentsCollection.document(commentId);
            commentDocRef.delete();

            return ResponseEntity.ok("Comment deleted successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to delete comment: " + e.getMessage());
        }
    }

    public ResponseEntity<?> getAllComments(String id) {
        try {
            CollectionReference videosCollection = dbFirestore.collection(COLLECTION_NAME);
            DocumentReference videoDocRef = videosCollection.document(id);
            CollectionReference commentsCollection = videoDocRef.collection("comments");

            ApiFuture<QuerySnapshot> future = commentsCollection.get();
            QuerySnapshot querySnapshot = future.get();
            List<Comment> commentList = new ArrayList<>();

            for (QueryDocumentSnapshot document : querySnapshot.getDocuments()) {
                Comment comment = document.toObject(Comment.class);
                commentList.add(comment);
            }

            return ResponseEntity.ok(commentList);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve comments: " + e.getMessage());
        }
    }
}
