package com.lahitani.tiktokappapi.entity;

import java.util.HashMap;
import java.util.Map;

public class User {
	private String name;
	private String profilePhoto;
	private String email;
	private  String uid;
	
		
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getProfilePhoto() {
		return profilePhoto;
	}


	public void setProfilePhoto(String profilePhoto) {
		this.profilePhoto = profilePhoto;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getUid() {
		return uid;
	}


	public void setUid(String uid) {
		this.uid = uid;
	}


		public Map<String, Object> toMap() {
	        Map<String, Object> map = new HashMap<>();
	        map.put("name", name);
	        map.put("uid", uid);
	        map.put("email", email);
	        map.put("profilePhoto", profilePhoto);
	        return map;
	    }
}
