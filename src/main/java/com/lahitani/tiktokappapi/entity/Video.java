package com.lahitani.tiktokappapi.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Video {
    private String username;
    private String uid;
    private String id;
    private List<String> likes;
    private int commentCount;
    private int shareCount;
    private String songName;
    private String caption;
    private String videoUrl;
    private String thumbnail;
    private String profilePhoto;

    public Video() {
        // Diperlukan konstruktor default kosong untuk kompatibilitas Firebase Firestore
    }

    public Video(String username, String uid, String id, List<String> likes, int commentCount, int shareCount,
            String songName, String caption, String videoUrl, String thumbnail, String profilePhoto) {
        this.username = username;
        this.uid = uid;
        this.id = id;
        this.likes = likes;
        this.commentCount = commentCount;
        this.shareCount = shareCount;
        this.songName = songName;
        this.caption = caption;
        this.videoUrl = videoUrl;
        this.thumbnail = thumbnail;
        this.profilePhoto = profilePhoto;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getLikes() {
        return likes;
    }

    public void setLikes(List<String> likes) {
        this.likes = likes;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getShareCount() {
        return shareCount;
    }

    public void setShareCount(int shareCount) {
        this.shareCount = shareCount;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("username", username);
        map.put("uid", uid);
        map.put("id", id);
        map.put("likes", likes);
        map.put("commentCount", commentCount);
        map.put("shareCount", shareCount);
        map.put("songName", songName);
        map.put("caption", caption);
        map.put("videoUrl", videoUrl);
        map.put("thumbnail", thumbnail);
        map.put("profilePhoto", profilePhoto);
        return map;
    }
}
