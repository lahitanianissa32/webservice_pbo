package com.lahitani.tiktokappapi.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Comment {
    private String username;
    private String comment;
    private Date datePublished;
    private List<String> likes;
    private String profilePhoto;
    private String uid;
    private String id;

    public Comment() {
        // Diperlukan konstruktor default kosong untuk kompatibilitas Firebase Firestore
    }

    public Comment(String username, String comment, Date datePublished, List<String> likes, String profilePhoto,
            String uid, String id) {
        this.username = username;
        this.comment = comment;
        this.datePublished = datePublished;
        this.likes = likes;
        this.profilePhoto = profilePhoto;
        this.uid = uid;
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(Date datePublished) {
        this.datePublished = datePublished;
    }

    public List<String> getLikes() {
        return likes;
    }

    public void setLikes(List<String> likes) {
        this.likes = likes;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("username", username);
        map.put("comment", comment);
        map.put("datePublished", datePublished);
        map.put("likes", likes);
        map.put("profilePhoto", profilePhoto);
        map.put("uid", uid);
        map.put("id", id);
        return map;
    }
}
