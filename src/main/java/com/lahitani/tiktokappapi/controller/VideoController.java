package com.lahitani.tiktokappapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.lahitani.tiktokappapi.entity.Video;
import com.lahitani.tiktokappapi.service.VideoService;

@RestController
@RequestMapping("/api/videos")
public class VideoController {
    private final VideoService videoService;

    @Autowired
    public VideoController(VideoService videoService) {
        this.videoService = videoService;
    }

    @PostMapping
    public ResponseEntity<String> saveVideo(@RequestBody Video video) {
        try {
            return videoService.saveVideo(video);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to save video: " + e.getMessage());
        }
    }

    @GetMapping("/{uid}")
    public ResponseEntity<?> getVideo(@PathVariable String uid) {
        try {
            return videoService.getVideoDetails(uid);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve video details: " + e.getMessage());
        }
    }

    @PutMapping("/{uid}")
    public ResponseEntity<String> updateVideo(@PathVariable String uid, @RequestBody Video video) {
        try {
            video.setUid(uid);
            return videoService.updateVideo(video);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to update video: " + e.getMessage());
        }
    }

    @DeleteMapping("/{uid}")
    public ResponseEntity<String> deleteVideo(@PathVariable String uid) {
        try {
            return videoService.deleteVideo(uid);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to delete video: " + e.getMessage());
        }
    }
    
    @GetMapping
    public ResponseEntity<?> getAllVideos() {
        try {
            return videoService.getAllVideos();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve videos: " + e.getMessage());
        }
    }
}
