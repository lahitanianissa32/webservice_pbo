package com.lahitani.tiktokappapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.lahitani.tiktokappapi.entity.Comment;
import com.lahitani.tiktokappapi.service.CommentService;

@RestController
@RequestMapping("/api/videos/{uid}/comments")
public class CommentController {
    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping
    public ResponseEntity<String> saveComment(@PathVariable String uid, @RequestBody Comment comment) {
        try {
            return commentService.saveComment(uid, comment);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to save comment: " + e.getMessage());
        }
    }

    @GetMapping("/{commentId}")
    public ResponseEntity<?> getComment(@PathVariable String uid, @PathVariable String commentId) {
        try {
            return commentService.getCommentDetails(uid);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve comment details: " + e.getMessage());
        }
    }

    @PutMapping("/{commentId}")
    public ResponseEntity<String> updateComment(@PathVariable String uid, @PathVariable String commentId, @RequestBody Comment comment) {
        try {
            comment.setUid(uid);
            comment.setId(commentId);
            return commentService.updateComment(comment);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to update comment: " + e.getMessage());
        }
    }

    @DeleteMapping("/{commentId}")
    public ResponseEntity<String> deleteComment(@PathVariable String uid, @PathVariable String commentId) {
        try {
            return commentService.deleteComment(uid, commentId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to delete comment: " + e.getMessage());
        }
    }
    
    @GetMapping
    public ResponseEntity<?> getAllComments(@PathVariable String uid) {
        try {
            return commentService.getAllComments(uid);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve comments: " + e.getMessage());
        }
    }
}
